from multiprocessing import context
from django.shortcuts import reverse ,redirect, render
from flask import request
from .models import *
from django.views.generic import *
from .forms import *
from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ValidationError
from django.urls import reverse_lazy, reverse
from django.http import HttpResponse, JsonResponse

#### For Rest Frame Work ###
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework import viewsets
from .serializer import *
from django.db.models import Q, query
from rest_framework import generics, mixins
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.shortcuts import reverse ,redirect, render
from django.views.generic import *

class HomeView(TemplateView):
    template_name = 'home.html'

class Register(CreateView):
    template_name = 'register.html'
    form_class = RegistrationForm

    def post(self, request):
        if request.method == 'POST':
            email = request.POST.get('email')
            print(email)
            username = email
            phonenumber = request.POST.get('phone')
            fullname = request.POST.get('fullname')
            print(fullname)
            address = request.POST.get('address')
            profilepicture = request.POST.get('profilepicture')
            password = request.POST.get('password')
            print(password)
            if User.objects.filter(phonenumber=phonenumber).exists():
                return render(self.request, self.template_name, {"form": self.form_class, "error": "Phone Number already taken!"})
            elif User.objects.filter(email=email).exists():
                return render(self.request, self.template_name, {"form": self.form_class, "error": "Email already taken!"})
            else:
                user = User.objects.create_user(username, phonenumber,email,password)
                user.fullname= fullname
                user.address= address
                user.profilepicture= profilepicture
                user.save()
                login(self.request, user)
                print(login(self.request, user))
                return render(request, 'home.html')
        else:
            pro = User.objects.all()
            return render(request, 'home.html', {'prob':pro})

class LoginView(FormView):
    template_name = "login.html"
    form_class = LoginForm
    success_url = reverse_lazy("demoapp:home")

    def form_valid(self, form):
        uname = form.cleaned_data.get("email_Or_phonenumber")
        pword = form.cleaned_data["password"]
        if '@' in uname:
            print(uname)
            user = User.objects.get(email=uname).username
            print(user)
        else:
            print("hello", uname)
            user = User.objects.get(phonenumber=uname).username
            print(user)
        usr = authenticate(username=user, password=pword)
        print(usr)
        if usr is not None:
            login(self.request, usr)
            #messages.success(self.request, 'Welcome')
        else:
            # messages.success(self.request, 'Invalid')
            return render(self.request, self.template_name, {"form": self.form_class, "error": "Invalid credentials"})

        return super().form_valid(form)

class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect("demoapp:home")

class File_upload(CreateView):
    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            my_file=request.FILES.get('file')
            pk = request.POST.get('appid')
            point = AddApp.objects.get(id=pk).points
            print(point)
            if TotalPoint.objects.filter(user=self.request.user).exists():
                if TotalPoint.objects.filter(app=AddApp.objects.get(id=pk)).exists():
                    pass
                else:
                    TotalPoint.objects.create(point=point, app=AddApp.objects.get(id=pk), user=self.request.user)
                    Image.objects.create(image=my_file, uploadedby = request.user)
            else:
                TotalPoint.objects.create(point=point, app=AddApp.objects.get(id=pk), user=self.request.user)
                Image.objects.create(image=my_file, uploadedby = request.user)
            # return HttpResponse('')
            return redirect("demoapp:home")
        return JsonResponse({'post':'fasle'})

class AddAppView(CreateView):
    template_name = 'addapp.html'
    form_class = AddAppForm

    def post(self, request):
        if request.method == 'POST':
            image = request.POST.get('image')
            points = request.POST.get('points')
            cat = request.POST.get('category')
            name, category = Category.objects.get_or_create(name=cat)
            # print(category)
            catname= name.name
            print(catname)
            subcat = request.POST.get('subcategory')
            subname, subcategory = SubCategory.objects.get_or_create(subname=subcat)
            sub = subname.subname
            AddApp.objects.create(image=image, category=Category.objects.get(name=catname), subcategory=SubCategory.objects.get(subname=sub), points=points)
            return render(request, 'home.html')
        else:
            app = AddApp.objects.all()
            category = Category.objects.all()
            subcategory = SubCategory.objects.all()
            return render(request, 'home.html', {'app':app, 'category':category, 'subcategory':subcategory})

class MyDetail(TemplateView):
    template_name = 'detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['details'] = User.objects.get(id=self.request.user.id)
        return context

class AllAppDisplay(TemplateView):
    template_name = 'allappdisplay.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['allapp'] = AddApp.objects.all()
        return context

class AppDetailView(TemplateView):
    template_name = "appdetail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        url_slug = self.kwargs['id']
        product = AddApp.objects.get(id=url_slug)
        context['product'] = product
        return context

from django.db.models import Sum
class MyPoints(TemplateView):
    template_name = 'mypoints.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        mypoints = TotalPoint.objects.filter(user=self.request.user)
        total_prices = sum(product.point for product in mypoints)
        context['mypoints'] = mypoints
        context['total'] = total_prices
        return context

class WorkDone(TemplateView):
    template_name = 'workdone.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        mypoints = TotalPoint.objects.filter(user=self.request.user)
        context['mypoints'] = mypoints
        return context

# Create your views here.

### For Rest API
class UserDetailView(mixins.CreateModelMixin, generics.ListAPIView):
	lookup_field = 'pk'
	serializer_class = UserDetailSerializer
	permission_classes = [IsAuthenticated]

	def get_queryset(self):
		return User.objects.filter(username=self.request.user.username)

	def post(self,request):
		serializer = self.serializer_class(data=request.data)
		serializer.is_valid(raise_exception=True)
		serializer.save()
		return Response(serializer.data, status=status.HTTP_200_OK)

class ImageUploadView(mixins.CreateModelMixin, generics.ListAPIView):
	lookup_field = 'pk'
	serializer_class = ImageSerializer
	permission_classes = [IsAuthenticated]

	def get_queryset(self):
		return Image.objects.filter(uploadedby=self.request.user)

	def post(self,request):
		serializer = self.serializer_class(data=request.data)
		serializer.is_valid(raise_exception=True)
		serializer.save()
		return Response(serializer.data, status=status.HTTP_200_OK)

class TotalView(mixins.CreateModelMixin, generics.ListAPIView):
	lookup_field = 'pk'
	serializer_class = TotalPointSerializer
	permission_classes = [IsAuthenticated]

	def get_queryset(self):
		return TotalPoint.objects.filter(user=self.request.user)

	def post(self,request):
		serializer = self.serializer_class(data=request.data)
		serializer.is_valid(raise_exception=True)
		serializer.save()
		return Response(serializer.data, status=status.HTTP_200_OK)

class AddAppView(mixins.CreateModelMixin, generics.ListAPIView):
	lookup_field = 'pk'
	serializer_class = AddAppSerializer
	permission_classes = [IsAuthenticated]

	def get_queryset(self):
		return AddApp.objects.all()
        
    def post(self,request):
        serializer = self.serializer_class(data=request.data)
        data = request.data
        image = data.get('image')
        points = data.get('points')
        cat = data.get('category')
        name, category = Category.objects.get_or_create(name=cat)
        catname= name.name
        subcat = data.get('subcategory')
        subname, subcategory = SubCategory.objects.get_or_create(subname=subcat)
        sub = subname.subname
        AddApp.objects.create(image=image, category=Category.objects.get(name=catname), subcategory=SubCategory.objects.get(subname=sub), points=points)
        return Response(status=status.HTTP_200_OK)
