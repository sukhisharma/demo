from distutils.command.upload import upload
from django.db import models

# Create your models here.
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin)

from django.db import models
from datetime import datetime, date, timedelta


class UserManager(BaseUserManager):

    def create_user(self, username, phonenumber, email, password=None):
        if username is None:
            raise TypeError('Users should have a Phone number')
        if email is None:
            raise TypeError('Users should have a Email')

        user = self.model(username=username, phonenumber=phonenumber, email=self.normalize_email(email))
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, phonenumber, email, password=None):
        if password is None:
            raise TypeError('Password should not be none')

        user = self.create_user(username,phonenumber, email, password)
        user.is_superuser = True
        user.is_staff = True
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=255, unique=True)
    phonenumber = models.CharField(max_length=255, blank=True, null=True, unique=True)
    fullname = models.CharField(max_length=200, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    profilepicture = models.ImageField(blank=True, null=True)
    email = models.EmailField(max_length=255, blank=True, null=True, unique=True)
    is_verified = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['phonenumber', 'email']
    # USERNAME_FIELD = 'email'
    # REQUIRED_FIELDS = ['username']

    objects = UserManager()

    def __str__(self):
        return str(self.fullname)

class Image(models.Model):
    image = models.ImageField(upload_to='images/')
    uploadedby = models.ForeignKey(User, on_delete=models.CASCADE)
    uploadedon = models.DateTimeField(auto_now_add=True)

class Category(models.Model):
    name = models.CharField(max_length=200)

class SubCategory(models.Model):
    subname = models.CharField(max_length=200)

class AddApp(models.Model):
    image = models.ImageField(upload_to='app/')
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    points = models.PositiveIntegerField()

class TotalPoint(models.Model):
    point = models.PositiveIntegerField()
    app = models.ForeignKey(AddApp, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user')


