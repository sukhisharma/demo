from django.contrib import admin
from .models import *

admin.site.register([User,Image,Category, SubCategory, AddApp, TotalPoint])

# Register your models here.
