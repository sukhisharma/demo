from django.urls import path
from .views import * 
from . import views

app_name = 'demoapp'
urlpatterns =[
    path('', HomeView.as_view(), name='home'),
    path('register/', Register.as_view(), name='register'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('upload/', File_upload.as_view(), name='upload'),
    path('addapp/', AddAppView.as_view(), name='addapp'),
    path('detail/', MyDetail.as_view(), name='detail'),
    path('allapp/', AllAppDisplay.as_view(), name='allapp'),
    path('appdetail/<int:id>/', AppDetailView.as_view(), name='appdetail'),
    path('mypoints/', MyPoints.as_view(), name='mypoints'),
    path('workdone/', WorkDone.as_view(), name='workdone'),

    ### For rest Api
    path('total/', TotalView.as_view(), name=''),
]