from django import forms
from .models import *
from django.contrib.auth import get_user_model
User = get_user_model()

class RegistrationForm(forms.ModelForm):

    class Meta:
        model = User
        fields = '__all__'#["username", "password", "email", "full_name", "address"]

    def clean_phonenumber(self):
        uphone = self.cleaned_data.get("phone")
        if User.objects.filter(phonenumber=uphone).exists():
            raise forms.ValidationError(
                "Customer with this Phone Number already exists.")

        return uphone

    def clean_email(self):
        umail = self.cleaned_data.get("email")
        if User.objects.filter(email=umail).exists():
            raise forms.ValidationError(
                "Customer with this email already exists.")

        return umail

class LoginForm(forms.Form):
    email_Or_phonenumber = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())

class AddAppForm(forms.ModelForm):
    class Meta:
        model = AddApp
        fields = '__all__'
